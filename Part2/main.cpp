#include <chrono>
#include <iostream>
#include <cstring>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <vector_types.h>

#include "openMP.hpp"
#include "CUDA_wrappers.hpp"
#include "common/image_helpers.hpp"


void prepareFilter(float **filter, int *filterWidth, float *filterSigma)
{
  static const int blurFilterWidth = 9;
  static const float blurFilterSigma = 2.;

  *filter = new float[blurFilterWidth * blurFilterWidth];
  *filterWidth = blurFilterWidth;
  *filterSigma = blurFilterSigma;

  float filterSum = 0.f;
  const int halfWidth = blurFilterWidth/2;

  for (int r = -halfWidth; r <= halfWidth; ++r) 
  {
    for (int c = -halfWidth; c <= halfWidth; ++c) 
    {
      float filterValue = expf( -(float)(c * c + r * r) / (2.f * blurFilterSigma * blurFilterSigma));
      (*filter)[(r + halfWidth) * blurFilterWidth + c + halfWidth] = filterValue;
      filterSum += filterValue;
    }
  }

  float normalizationFactor = 1.f / filterSum;

  for (int r = -halfWidth; r <= halfWidth; ++r) 
  {
    for (int c = -halfWidth; c <= halfWidth; ++c) 
    {
      (*filter)[(r + halfWidth) * blurFilterWidth + c + halfWidth] *= normalizationFactor;
    }
  }  
}


void freeFilter(float *filter)
{
  delete[] filter;
}

int main( int argc, char** argv )
{
  using namespace cv;
  using namespace std;  
  using namespace std::chrono;

  if( argc != 2)
  {
    cout <<" Usage: blur_image imagefile" << endl;
    return -1;
  }

  Mat image, blurredImage, referenceBlurredImage;
  uchar4 *imageArray, *blurredImageArray;

  prepareImagePointers(argv[1], image, &imageArray, blurredImage, &blurredImageArray, CV_8UC4);
  int numRows = image.rows, numCols = image.cols;

  float *filter, filterSigma;
  int filterWidth;
  prepareFilter(&filter, &filterWidth, &filterSigma);

  cv::Size filterSize(filterWidth, filterWidth);
  auto start = system_clock::now();
  cv::GaussianBlur(image, referenceBlurredImage, filterSize, filterSigma, filterSigma, BORDER_REPLICATE);
  auto duration = duration_cast<milliseconds>(system_clock::now() - start);
  cout<<"OpenCV time (ms):" << duration.count() << endl; 

  start = system_clock::now();
  BlurImageOpenMP(imageArray, blurredImageArray, numRows, numCols, filter, filterWidth);
  duration = duration_cast<milliseconds>(system_clock::now() - start);
  cout<<"OpenMP time (ms):" << duration.count() << endl; 
  cout<<"OpenMP similarity:" << getEuclidianSimilarity(referenceBlurredImage, blurredImage) << endl;

  for (int i=0; i<4; ++i)
  {
    memset(blurredImageArray, 0, sizeof(uchar4)*numRows*numCols); 
    start = system_clock::now();
    BlurImageCUDA(imageArray, blurredImageArray, numRows, numCols, filter, filterWidth);
    duration = duration_cast<milliseconds>(system_clock::now() - start);
    cout<<"CUDA time full (ms):" << duration.count() << endl;
    cout<<"CUDA similarity:" << getEuclidianSimilarity(referenceBlurredImage, blurredImage) << endl;
  }

  freeFilter(filter);

  return 0;
}

