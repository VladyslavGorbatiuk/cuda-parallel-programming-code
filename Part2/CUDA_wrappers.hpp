void BlurImageCUDA(const uchar4 * const h_image, 
				   uchar4 * const h_blurredImage, 
	   			   const size_t numRows, 
   				   const size_t numCols, 
				   const float * const h_filter, 
				   const size_t filterWidth);